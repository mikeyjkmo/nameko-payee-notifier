import pytest

from nameko.containers import ServiceContainer
from nameko.standalone.events import event_dispatcher
from nameko.testing.services import entrypoint_waiter
from nameko.testing.services import replace_dependencies

from payment_notifier.service import (
    PaymentNotifierService,
    PAYEE_NOTIFICATION_SUBJECT,
    PAYEE_NOTIFICATION_EMAIL)
from payment_notifier.mailgun import MailGunWrapper


@pytest.fixture()
def fake_payload(fake):
    """
    Create a payload for testing purposes.

    :param fake: fixure for Faker instance.
    """
    return {
        'client': {
            'name': fake.name(),
            'email': fake.safe_email()
        },
        'payee': {
            'name': "Mikey",
            'email': "mikeyjkmo@gmail.com"
        },
        'payment': {
            'amount': fake.random_int(),
            'currency': fake.random_element(
                ("USD", "GBP", "EUR")
            )
        }
    }


@pytest.fixture()
def mailgun_in_test(requests_session):
    """
    The MailGunWrapper with testmode enabled. API calls will still be sent
    to the production Mailgun service but e-mails will not actually be sent.

    :param requests_session: Fixture for a requests.Session() to use for
        this MailGunWrapper instance.
    """
    return MailGunWrapper(requests_session, params={"o:testmode": "Yes"})


def test_payment_notifier_payment_received(
        rabbit_config, mocker, fake_payload, mailgun_in_test):
    """
    Integration test PaymentNotifierService.payment_received()
    event handler.

    Test that sending a payment_received event will lead
    to the Mailgun service being called with the expected
    e-mail message.

    :param rabbit_config: Fixture for the RabbitMQ configuration instance.
    :param mocker: Fixture of pytest mocker instance used to spy on
        the call on MailGunWrapper.send_email.
    :param fake_payload: Fixture for fake payment payload data.
    :param mailgun_in_test: Fixture for the MailGunWrapper test instance.
    """
    mocker.spy(mailgun_in_test, "send_email")
    container = ServiceContainer(PaymentNotifierService, rabbit_config)
    replace_dependencies(container, mailgun=mailgun_in_test)
    container.start()

    dispatch = event_dispatcher(rabbit_config)

    with entrypoint_waiter(container, 'payment_received'):
        dispatch("payments", "payment_received", fake_payload)

    assert mailgun_in_test.send_email.call_count == 1

    expected_recipient = "{} <{}>".format(
        fake_payload["payee"]["name"], fake_payload["payee"]["email"])
    expected_subject = PAYEE_NOTIFICATION_SUBJECT.format(
        fake_payload["client"]["name"])
    expected_body = PAYEE_NOTIFICATION_EMAIL.format(
        payee=fake_payload["payee"]["name"],
        amount=fake_payload["payment"]["amount"],
        currency=fake_payload["payment"]["currency"],
        client=fake_payload["client"]["name"],
        email=fake_payload["client"]["email"]
    )

    mailgun_in_test.send_email.assert_called_with(
        expected_recipient, expected_subject, expected_body)