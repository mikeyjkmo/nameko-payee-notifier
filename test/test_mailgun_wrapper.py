import textwrap
import uuid

import pytest
import requests
import responses

from urllib.parse import urlencode

from payment_notifier.mailgun import (
    MailGunWrapper, MAILGUN_SANDBOX_FROM, MAILGUN_API_MESSAGES_URL)


@responses.activate
@pytest.mark.parametrize(
        "status_code,message,expected_result",
        [(200, "Queued. Thank You.", "ok"),
         (400, "Request is malformed.", "error"),
         (500, "Internal server error.", "error")])
def test_mailgun_send_email(fake, status_code, message, expected_result):
    """
    Unit test for MailGunWrapper.send_email().

    :param fake: fixture of Faker instance.
    :param status_code: pytest parameter for HTTP status code to simulate and
        test
    :param message: pytest parameter for HTTP response message to simulate and
        test
    :param expected_result: pytest parameter for the expected status of the
       send_email() call.
    """
    # For some reason, the mailgun API doesn't return requests-parsable JSON...
    responses.add(
        responses.POST,
        MAILGUN_API_MESSAGES_URL,
        body=str({
            "id": uuid.uuid4(),
            "message": message
        }),
        status=status_code
    )
    session = requests.Session()

    mailgun_wrapper = MailGunWrapper(session)

    recipient_name = fake.name()
    recipient = "{} <{}>".format(recipient_name, fake.safe_email())
    subject = "My Test Subject"

    body = textwrap.dedent(
        """
        Dear {}

        This is my test e-mail.

        bye!
        love from Bot
        """.format(recipient_name)
    ).strip()

    result = mailgun_wrapper.send_email(recipient, subject=subject, body=body)

    assert result["status"] == expected_result

    data = {
        "from": MAILGUN_SANDBOX_FROM,
        "to": recipient,
        "subject": subject,
        "text": body
    }

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == MAILGUN_API_MESSAGES_URL
    assert responses.calls[0].request.body == urlencode(data)
    assert responses.calls[0].request.method == "POST"
