import faker
import pytest
import requests


LOCALES_TO_TEST = ("en_GB", "en_US", "zh_CN",)


@pytest.fixture(
        scope="session", params=LOCALES_TO_TEST)
def fake(request):
    """
    Fixture for created the faker instance.

    Test sending emails with different locales, as student.com is an
    international service.
    """
    faker_instance = faker.Factory.create(locale=request.param)
    # Seeding for test consistency. Will change at controlled intervals
    faker_instance.seed(4321)
    return faker_instance


@pytest.fixture()
def requests_session():
    return requests.Session()
