import logging
import urllib

import requests

from nameko.extensions import DependencyProvider

DEV_LOGGER = logging.getLogger(__name__)

MAILGUN_API_BASE_URL = "https://api.mailgun.net/v3/"
MAILGUN_API_SANDBOX_URL = urllib.parse.urljoin(
    MAILGUN_API_BASE_URL,
    "sandboxe492a1c6b9474c2b9fa8c08a4a86fead.mailgun.org/")
MAILGUN_API_MESSAGES_URL = urllib.parse.urljoin(
    MAILGUN_API_SANDBOX_URL, "messages")
MAILGUN_API_KEY = "e39b4bc76ffabe5d4ddc69ce26ceb96a-770f03c4-710f1bf3"
MAILGUN_SANDBOX_FROM = (
    "Mailgun Sandbox "
    "<postmaster@sandboxe492a1c6b9474c2b9fa8c08a4a86fead.mailgun.org>")


class MailGunWrapper(object):
    """
    Wrapper for MailGun API.

    Sends requests to the MailGun API to perform tasks such as sending
    e-mails.
    """
    logger = DEV_LOGGER.getChild("mailgun_wrapper")

    def __init__(self, session, params=None):
        """
        :params session: requests.Session instance to use for this wrapper's
            requests.
        """
        self._session = session
        self._mailgun_params = params if params is not None else {}

    def _send_mailgun_request(self, method, url, data):
        """
        Send a request to the MailGun API.

        :param method: The HTTP method for the request.
        :param url: The URL for the API call.
        :param data: The payload data for the request.
        """
        data.update(self._mailgun_params)
        new_request = requests.Request(
            method,
            url,
            auth=("api", MAILGUN_API_KEY),
            data=data
        )
        prepped = new_request.prepare()
        return self._session.send(prepped)

    def send_email(self, recipients, subject, body, extra_params=None):
        """
        Send an e-mail using the temporary Mailgun sandbox account.

        :param recipients: One or more recipients to deliver the email to.
            This can either be a string for one recipient, or a list of
            strings for multiple recipients.
        :param subject: the subject for the email being sent
        :param body: The body for the email being sent
        :param extra_params: Any extra params to send as part of the MailGun
            API call.
        """
        data = {
            "from": MAILGUN_SANDBOX_FROM,
            "to": recipients,
            "subject": subject,
            "text": body
        }

        if extra_params:
            data.update(extra_params)

        response = self._send_mailgun_request(
            "POST", MAILGUN_API_MESSAGES_URL, data=data)

        result = {
            "status": "error", "message": "An unexpected error occurred."
        }

        try:
            response.raise_for_status()
            # For some reason, the mailgun API doesn't return requests-parsable
            # JSON...
            assert "Queued. Thank You." in response.text
        except (requests.exceptions.HTTPError, AssertionError):
            error_msg = (
                "An error occurred whilst trying to send an email. "
                "Recipients: {}. Response: {}").format(
                    recipients, response.text)
            result = {"status": "error", "message": error_msg}
            self.logger.exception(error_msg)
        else:
            result = {
                "status": "ok", "message": "E-mail was sent successfully"
            }
            self.logger.info("Succesfully sent e-mail to: %s", recipients)

        return result


class MailGunWebService(DependencyProvider):
    """
    Dependency provider for MailGunWrapper.
    """
    def setup(self):
        self._session = requests.Session()

    def get_dependency(self, worker_ctx):
        return MailGunWrapper(self._session)
