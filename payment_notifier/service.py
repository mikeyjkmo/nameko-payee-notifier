import logging
import textwrap
import sys

from marshmallow.exceptions import ValidationError
from nameko.events import event_handler

from payment_notifier.mailgun import MailGunWebService
from payment_notifier.schemas import PaymentReceivedSchema

DEV_LOGGER = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout)

PAYEE_NOTIFICATION_EMAIL = textwrap.dedent(
    """
    Dear {payee},

    You have received a payment of {amount} {currency} from {client} ({email}).

    Yours,
    student.com
    """
).strip()
PAYEE_NOTIFICATION_SUBJECT = "Payment received from {}"


class PaymentNotifierService(object):
    """
    Nameko service for sending payment notifications to payees.
    """
    name = "payment_notifier"
    mailgun = MailGunWebService()
    logger = DEV_LOGGER.getChild(name)
    payment_received_schema = PaymentReceivedSchema()

    @event_handler("payments", "payment_received")
    def payment_received(self, payload):
        """
        Event handler for "payment_received" events coming from the "payments"
        service.

        :param payload: A dictionary container the information about the
            payment, and the payee to notify.
        """
        try:
            payment = self.payment_received_schema.load(payload)
        except ValidationError:
            self.logger.exception(
                "Cannot send e-mail notification for payment as details are "
                "malformed. Payload: %s", payload)

        self.logger.info(
            "Payment received from: %s <%s>",
            payment.data["client"]["name"], payment.data["client"]["email"])

        body = PAYEE_NOTIFICATION_EMAIL.format(
            payee=payment.data["payee"]["name"],
            amount=payment.data["payment"]["amount"],
            currency=payment.data["payment"]["currency"],
            client=payment.data["client"]["name"],
            email=payment.data["client"]["email"]
        )

        return self.mailgun.send_email(
            recipients="{} <{}>".format(
                payment.data["payee"]["name"],
                payment.data["payee"]["email"]),
            subject=PAYEE_NOTIFICATION_SUBJECT.format(
                payment.data["client"]["name"]),
            body=body
        )
