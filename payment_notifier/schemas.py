from marshmallow import Schema, fields


class PersonSchema(Schema):
    """
    Marshmallow schema for a person.
    """
    name = fields.String()
    email = fields.Email()


class PaymentSchema(Schema):
    """
    Marshmallow schema for a payment.
    """
    amount = fields.Integer()
    currency = fields.String()


class PaymentReceivedSchema(Schema):
    """
    Marshmallow schema for a payment_received payload.
    """
    client = fields.Nested(PersonSchema)
    payee = fields.Nested(PersonSchema)
    payment = fields.Nested(PaymentSchema)
