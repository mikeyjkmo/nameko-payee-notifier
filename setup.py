from setuptools import find_packages, setup


setup(
    name='payment-notifier-service',
    version='0.0.1',
    description='Notify payees when they receive a payment.',
    author='mikeyjkmo',
    packages=find_packages(exclude=['test', 'test.*']),
    py_modules=['payment_notifier'],
    install_requires=[
        "nameko",
        "requests",
        "marshmallow",
    ],
    extras_require={
        'dev': ['pytest', 'pytest-mock', 'faker','responses']
    }
)
